### Resources
- [NodeJS YAML](https://eemeli.org/yaml/#documents)
- [Express JS](https://expressjs.com/en/5x/api.html#res)

### Running
`$env:PORT=<port>; $env:DEBUG="backend:*"; npm start`