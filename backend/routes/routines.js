var express = require('express');
var router = express.Router();
var YAML = require('yaml');
var fs = require('fs');

var routinePath = './public/routines/bia-routines.yaml';
var file = fs.readFileSync(routinePath, 'utf8');
var routines = YAML.parseDocument(file);



/* GET routines page. */
router.get('/', function (req, res, next) {
    res
        .set("Access-Control-Allow-Origin", "*")
        .send(routines.toJSON());
});

module.exports = router;
