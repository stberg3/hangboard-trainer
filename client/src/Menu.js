import React, {Component} from 'react'
import {ButtonGroup, Button, Row, Container } from 'react-bootstrap'

class Menu extends Component {
    constructor(props) {
        super(props)    
        this.handleSelect = this.handleSelect.bind(this)
    }

    handleSelect(selected) {
        this.props.handleSelect(selected)
    }

    render() {
        const levels = this.props.levels

        return (
                <ButtonGroup vertical="sm" className="w-100 pl-2">            
                    {levels.map((level) =>             
                        <Button key={level} active={level === this.props.selected}
                            onClick={() => this.handleSelect(level)}>
                            {level}
                        </Button>                  
                    )}
                </ButtonGroup>
)
    }
}

export default Menu