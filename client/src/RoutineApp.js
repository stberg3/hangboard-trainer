import React, { Component } from 'react';
import Routine from './Routine'
import Menu from './Menu'
import Timer from './Timer'
import { Container, Row, Col, ProgressBar, Spinner } from 'react-bootstrap';

const DEBUG_MULTIPLIER = 1000

class RoutineApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            minute: 0,
            time: 0,
            started: false            
        };
        this.handleSelect = this.handleSelect.bind(this)
        this.startTimer = this.startTimer.bind(this)
        this.stopTimer = this.stopTimer.bind(this)
        this.tick = this.tick.bind(this)
    }

    tick() {
        let msInAMinute = 60*1000
        let minute = (this.state.time - this.state.time % msInAMinute) / (msInAMinute)         
        
        // every minute
        if(minute !== this.state.minute) {            
            console.log("Time:      " + this.state.time)
            console.log("Minute:    " + this.state.minute)
            if(minute >= this.state.routines[this.state.selected].length) {
                this.stopTimer()
            }
        }
        

        if(this.state.started) {
            this.setState({
                minute: minute,
                time: this.state.time + (1 * DEBUG_MULTIPLIER)
            })
        }
    }

    stopTimer(){
        console.log("RoutineApp.stopTimer()")
        clearInterval(this.state.interval)
        this.setState({
            time: 0,
            minute: 0,
            interval: null,
            started: false
        })

        console.log("RoutineApp.state:")
        console.dir(this.state)
    }

    startTimer() {
        console.log("RoutineApp.startTimer()")
        if(this.state.started) {
            clearInterval(this.state.interval)
        } else {
            this.setState({
                interval: setInterval(this.tick, 1)
            })
        }

        this.setState({started: !this.state.started})
    }

    componentDidMount() {
        fetch("http://localhost:9000/routines/")
            .then((routines) => { return routines.json(); })
            .then(
                (routines) => {
                    let selected = Object.keys(routines)[0]
                    this.setState({
                        isLoaded: true,
                        routines: routines,
                        selected: selected
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error: error,
                        routines: null
                    });
                }
            )
    }
    
    handleSelect(selected) {        
        if(this.state.selected === selected){
            return
        }
        
        clearInterval(this.state.interval)
        this.setState({
            selected: selected,
            minute: 0,
            time: 0
        })
    }

    render() {        
        if(!this.state.isLoaded) {
            return <Spinner animation="grow" />
        }

        const routines = this.state.routines;
        var levels = Object.keys(routines);

        return (
            <Container fluid className="RoutineApp mt-2 h-100">
                <Row>                
                    <Col xs={3} className="w-100">
                        <Row className="justify-content-md-center">
                            <Timer                                 
                                startTimer={this.startTimer}
                                stopTimer={this.stopTimer}
                                time={this.state.time}
                                started={this.state.started}
                                routine={this.state.routines[this.state.selected]}
                                />          
                        </Row>
                        <Row className="justify-content-md-center mt-1">
                            <Menu
                                levels={levels} 
                                handleSelect={this.handleSelect}
                                selected={this.state.selected}
                                />
                        </Row>
                    </Col>                
                    <Col xs={9} >
                        <Routine className="mh-100" 
                            minute={this.state.minute} 
                            routine={this.state.routines[this.state.selected]}  />                        
                    </Col>    
                </Row>
            </Container>
        );
    }
}

export default RoutineApp;
