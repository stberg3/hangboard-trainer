import React, {Component} from 'react'
import { ButtonGroup, Button, Row, Col, Container, ProgressBar} from 'react-bootstrap'

class Timer extends Component {
    constructor(props) {
        super(props)
        this.startTimer = this.startTimer.bind(this)
        this.stopTimer = this.stopTimer.bind(this)
    }

    startTimer(e) {
        console.log("starting the timer...")
        this.props.startTimer()
    }

    stopTimer(e) {
        console.log("stopping the timer...")
        this.props.stopTimer()
    }

    formatTime() {
        let dateTime = new Date(this.props.time)
        let min = dateTime.getMinutes().toString().padStart(2, '0')
        let sec = dateTime.getSeconds().toString().padStart(2, '0')
        let ms = dateTime.getMilliseconds().toString().padStart(2, '0').substr(0, 2)

        return <>{min}:{sec}:{ms}</>
    }

    render() {
        return (
            <Col>     
                <Row className="justify-content-md-center">
                <ButtonGroup className="w-100 ml-2">
                    <Button id="start-button" onClick={this.startTimer}>START</Button> 
                    <Button id="stop-button" onClick={this.stopTimer}>STOP</Button>                
                </ButtonGroup>
                    </Row>     
                <Row id="timer" className="justify-content-md-center">
                    <Col>
                        <h1>{this.formatTime()}</h1>
                    </Col>
                </Row>
                <ProgressBar now={100*((this.props.time%60000)/60000)} />            
            </Col>
        )
    }
}

export default Timer