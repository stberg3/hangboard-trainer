import React, {Component} from 'react'
import { Col, Row, ListGroupItem, ListGroup, Carousel, CarouselItem, Container  } from 'react-bootstrap'

class Routine extends Component {
    constructor(props) {
        super(props)
        this.state = {
            routine: this.props.routine,
            minute: this.props.minute
        }
        this.switchRoutine = this.switchRoutine.bind(this)
        this.TaskRow = this.TaskRow.bind(this)
    }

    TaskRow({ minute, tasks}) {        
        return (
                    <ListGroupItem key={minute} active={minute === this.props.minute + 1}>     
                        <Row>
                        <Col xs={1} >
                            {minute}
                        </Col>
                        { tasks.map( 
                            (task) => 
                            <Col>
                                {task}
                            </Col>
                        )}
                        </Row>           
                    </ListGroupItem>      
        )
    }

    switchRoutine(routine) {
        this.setState({
            routine: routine
        })
    }

    render() {
        const routine = this.props.routine;

        return (
            <ListGroup 
                className="h-100"
                controls={false} 
                activeIndex={this.props.minute}>
                {routine.map(
                    (tasks, minute) =>          
                        <this.TaskRow
                            key={minute}
                            minute={minute + 1}
                            tasks={tasks.tasks}
                            />
                )}
            </ListGroup>
        );
    }
}

export default Routine
